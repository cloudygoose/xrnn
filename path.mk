#Get from /opt/intel/mkl/bin/mklvars.sh ia32 verbose
#MKLINCLUDE = -I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include
#MKL_FLAGS = -lmkl_intel -lmkl_intel_thread -lmkl_core -lm -liomp5 -lpthread -lm
#MKL_LIB = -L/opt/intel/composer_xe_2013_sp1.0.080/compiler/lib/ia32 -L/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/ia32
#CFLAGS = -m32 -O2 -Wall

#Get from /opt/intel/mkl/bin/mklvars.sh intel64 ilp64 verbose
MKL_LIB = -L/opt/intel/composer_xe_2013_sp1.0.080/compiler/lib/intel64 -L/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/intel64 -L/opt/intel/composer_xe_2013_sp1.0.080/compiler/lib/mic -L/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/mic
MKL_FLAGS = -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -lm -liomp5 -lpthread -lm
MKLINCLUDE = -I/opt/intel/composer_xe_2013_sp1.0.080/mkl/include
CFLAGS = -m64 -O2 -Wall
