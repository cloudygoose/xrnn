#the X(Xie) Recurrent Neural Network (Language Model) toolkit

##Motivation

Trying to reproduce Xie Chen's work in his papaer 'Efficient GPU-based Training of Recurrent Neural Network Language Models Using Spliced Sentence Bunch', published in Interspeech 2014.

##Makefile

add 'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/intel/composer_xe_2013_sp1.0.080/compiler/lib/ia32:/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/ia32' to your ~/.bashrc

first "make all" to compile

then "make run" to test

Attention:The 'float' or 'double' type are decided in GLib.h, this descision will also change the mkl blas function names.  

Attention:The dependencies in the Makefile now is incomplete, please use make clean before compiling.

##Version control

Done by git.

A very important feature 'wordToChar' is implemented in the tag 'icassp2014-wordToChar', I decided to delete this part of code in the master branch to keep the code clean.

##Data

We assume that there's no '<s>' or '</s>' in the text file and that '\n' delimits a sentence.

##Implementation notes

If the option "-cvocab" is set, then word will split to chars.

Many components are taken from the GNLM toolkit, also developed by the author.  

Constants like 'MAX_WORD_LENGTH' of the program can be found in src/GLib.cpp.  

GVocab should be regarded as part of XRNN(word vector).  

XNFeed ignores empty sentence.

Word id '-1' gives by XNFeed means zero word vector. It happens when a stream doesn't have any more words.  

XNFeed will return the id of '<unk>' if it meets a word not found in vocab(not <unk> either).

##Lessons learned during developing XRNN
For libraries like blas that have different functions for 'float' and 'double' types, we can use '#define' to unify the naming, see GLib.h  

Intel mkl's config file /opt/intel/mkl/bin/mklvars.sh is used to set a few environment variable before compiling, we can type use the 'verbose' set like : '/opt/intel/mkl/bin/mklvars.sh ia32 verbose', to find out what variables it has set.

When you want to convert a string to a float, you need to use 'atof', not 'atoi'.

##Author
Goose He   
Shanghai Jiaotong University   
SpeechLab
