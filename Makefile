test_args = -batch_size 5 -rand_seed 2 -debug_level 4 -train_file testing/text -valid_file testing/text -print_vocab testing/vocab_out -hidden_size 20 -unk 1 -bptt 1 -alpha 0.1 -debug_pause 100000 -output_lm_file testing/lm_file

all : mkbin xrnn

xrnn : mkbin
	cd src && make
	cp src/XRNN_train bin/
	cp src/XRNN_test bin/

mkbin :
	mkdir -p bin

clean : 
	cd src && make clean
	rm -rf bin
	
gdb :
	@echo ==================test==============
	gdb -ex=r --args ./bin/XRNN_train $(test_args)

test : 
	@echo ==================test==============
	./bin/XRNN_train
	@echo ==================test==============
	./bin/XRNN_train $(test_args)	
	
	
