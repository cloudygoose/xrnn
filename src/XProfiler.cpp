#include "XProfiler.h"
#include "GLib.h"
#include <string.h>

XProfiler::XProfiler() {
    names = new std::vector<char*>();
    time = new std::vector<double>();
    last_time = new std::vector<double>();
    n = 0;
}

void XProfiler::start(char *name) {
    int k = 0;
    for (k = 0;k < n;k++)
        if (strcmp(names->at(k), name) == 0)
            break;
    if (k == n) {
        time->push_back(0);
        last_time->push_back(-1);
        char *nn = new char[100];
        strcpy(nn, name);
        names->push_back(nn);
        n++;
    }
    if (last_time->at(k) > 0)
        gerr("XProfiler::start event %s restarted.\n", name);
    (*last_time)[k] = std::clock();
}

void XProfiler::stop(char *name) {
    int k = 0;
    for (k = 0;k < n;k++) {
        if (strcmp(names->at(k), name) == 0)
            break;
    }
    if (k == n) 
        gerr("XProfiler::stop event %s not found.\n", name);
    if (last_time->at(k) < 0)
        gerr("XProfiler::stop event %s not started.\n", name);
    (*time)[k] += (std::clock() - last_time->at(k)) / (double)CLOCKS_PER_SEC; 
    (*last_time)[k] = -1;
}

void XProfiler::printToLog() {
    for (int i = 0;i < n;i++) {
        glog(3, "XProfiler::printToLog event %s runs for %lf seconds.\n", names->at(i), time->at(i));
    }
}
