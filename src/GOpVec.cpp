#include "GOpVec.h"
#include "GLib.h"
#include <string.h>
#include <stdio.h>
GOp::GOp(const char *n, const char *t, const char* s, const char* de, bool ne) { //ne defaults to be true
    name = n;
    ty = t;
    str = s;
    get = "[NOT SET]";
    def = de;
    how = "[USER]";
    need = ne;
}
GOpVec::GOpVec() {
    ops = new std::vector<GOp*>();
}
void GOpVec::insert(GOp* op) {
    ops->insert(ops->end(), op);
}
std::vector<GOp*>::iterator GOpVec::begin() {
    return ops->begin();
}
std::vector<GOp*>::iterator GOpVec::end() {
    return ops->end();
}
const char* GOpVec::findOp(const char* name) {
    std::vector<GOp*>::iterator it = ops->begin();
    while (it != ops->end()) {
        GOp *op = *it;
        if (!strcmp(op->name, name))
            return op->get;
        it++;
    }
    return NULL; 
}
void GOpVec::sweepArgs(int argc, char **argv) {
    std::vector<GOp*>::iterator it = ops->begin();
    while (it != ops->end()) {
        GOp *op = *it; 
        it++;
        int f;
        for (f = 1;f < argc;f++) {
            if (!strcmp(op->name, argv[f] + 1)) //pass the '-'
                break;
        }
        if (f < argc - 1) {
            op->get = argv[f + 1];
        }
        else {
            if (op->need && op->def == NULL)
            {
                gerr("option %s not found\n", op->name);                
            }
            op->get = op->def;
            op->how = "[DEFAULT]";
        }
    }
}
