#include "XNFeed.h"
#include <stdlib.h>
#include <string.h>

XNFeed::XNFeed(int bs, int ind, GVocab *voc) {
	vocab = voc;
	batch_size = bs;
	independent = ind;
	streams = new std::vector<std::list<int>*>();
	for (int i = 0;i < batch_size;i++) {
		std::list<int> *li = new std::list<int>();
		streams->push_back(li);
		if (vocab->findWord("</s>") == -1)
			gerr("XNFeed::XNFeed can't find </s> in vocab.\n");
		li->push_back(vocab->findWord("</s>"));
	}
	file = NULL;
}

void XNFeed::initialize(const char *fn) {
	if (file != NULL) {
		gerr("XNFeed::initialize file already not NULL\n");
	}
	file = fopen(fn, "r");		
}

bool XNFeed::refreshStream(int ind) {
	int k;
	char buffer[MAX_WORD_LENGTH];
	std::list<int> *s = streams->at(ind);
	if (s->empty()) {
		int sen_get = 1; //if independent is 1, we only get one sentence, otherwise get batch_size * 100 consecutive sentences at one time
		if (independent == 0)
			sen_get = FEED_CONSECUTIVE_SENTENCE;

		for (int i = 0;i < sen_get;i++) {
			if (file == NULL) //File will be set to NULL when it expires
				if (i == 0)
					return false;
				else
					return true;

			vocab->readWord(buffer, file);

			while ((strlen(buffer) != 0) && (strcmp(buffer, "</s>") == 0)) { //Ignore empty sentence
				vocab->readWord(buffer, file);
			}

			if (strlen(buffer) == 0) { //File expires
				file = NULL;
				if (i == 0)
					return false;
				else
					return true;
			}

			do {
				k = vocab->findWord(buffer);
				if (k == -1)
					k = vocab->getUnkInd();
				s->push_back(k);
				vocab->readWord(buffer, file);
			} while (strcmp(buffer, "</s>") && strlen(buffer) > 0);
			s->push_back(vocab->findWord("</s>"));
			if (strlen(buffer) == 0) {
				fclose(file);
				file=NULL;
				break;
			}
		}
		
		return true;
	} else return true;
}

bool XNFeed::nextBatch(int *des) {
	bool goon = false;
	for (int i = 0;i < batch_size;i++) {
		//printf("strems %d has %d...\n", i, streams->at(i)->size());
		if (refreshStream(i)) {
			goon = true;
			des[i] = streams->at(i)->front();
			streams->at(i)->pop_front();
		} else des[i] = -1; //-1 means the stream is empty now
	}
	return goon;
}
