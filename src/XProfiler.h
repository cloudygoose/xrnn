#include <ctime>
#include <cstdio>
#include <vector>

class XProfiler {
    private:
        int n; //How many events are recorded
        std::vector<char*> *names;
        std::vector<double> *time;
        std::vector<double> *last_time; //Saves the last time(start) for an event
    public:
        XProfiler();
        void start(char *name);
        void stop(char *name);
        void printToLog(); //Print using glog
};
