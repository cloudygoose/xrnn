#include "GLib.h"

#include <stdio.h>
#include <vector>
#ifndef _GVOCAB_H_
#define _GVOCAB_H_

extern const int MAX_WORD_LENGTH;

class GWord {
    public:
        int len; //Length, for cvocab
        char *word;
        int cla; //The class the word is assigned to by GVocab::assignClass
		int count; //The count of the word when doing learnVocabFromFile
        WEIGHT_TYPE *vec;
        GWord(const char *w, int s);
};

class GVocab { //</s> and <unk> will be in vocab when it's constructed.
    private:
        int vec_size;
		int class_n;
        std::vector<GWord*> *words;

        void rehash(); //Rearrange the hash_table according to the words in the vector
		int *classLen; //The size of each class, len(class 0) is the number of classes
		int *classBegin; //The beginning index of each class in the output layer, which is just the index of the word in the vocab
        int *hash_table; //The hash table would contain all words, it need to be re-arranged if changes are made to the vocab.
        void addWordToHash(const char *w, int index);
        int addWord(const char *w); //Return the index of the word, if the word is added, the word's vector will also be initialized
		int addWV(const char *w, const WEIGHT_TYPE *v, int cou, int cla); //Add the word along with the vector and its class index, if the word exists, its vector will be over-writed.
    public:
        int getHash(const char *w); //Return the hash value of a word, already mod by VOCAB_HASH_SIZE
		GWord* getWord(int index);
        int findWord(const char *w); //Find a word(remember <unk> is also in vocab) according to the hash_table, important, if not found, return -1
        void readWord(char* word, FILE *f); //Read a word from file, \n is return as </s>, <unk> is returned as <unk>, user should provider a buffer
        void loadVocabFile(const char *f, bool add_su = true); //Add all words in file, whether plus </s> and <unk> to vocab
        int learnVocabFromFile(const char *f); //Return how many words are learned, the file will be fopened and fclosed, attention:word counts will not be recorded
        void countVocabFromFile(const char *f); //Will count vocab-count from this file
		int getWVFromFile(FILE *file); //Read Vocab from an ALREADY-FOPENED file, return the resulting vocab size
        void assignClass(); //Assign classes to words, classLen(int*), classBegin(int*) is also created in this function
        int size();
		int cn(); //Get class_n, the number of classes
		void shuffle(); //Randomly shuffle the vocab
		void sortByCount(); 
		int getCL(int c); //Get the length of class c
		int getCB(int c); //Get the beginning index of a class in the output layer
        int getVecSize();
        GVocab(int vs, int n); //vector_size, class_n, whether distinguish c_p and c_m during splitToChar
        void printVocab(const char *f);
		int getUnkInd();
        void tieToMatrix(WEIGHT_TYPE *mat);

};

#endif
