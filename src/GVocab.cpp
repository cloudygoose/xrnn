#include "GVocab.h"
#include "GLib.h"
#include <string.h>
#include <string>
#include <algorithm>
GWord::GWord(const char *w, int size) {
    word = new char[strlen(w) + 1];
    strcpy(word, w);
    cla = -1;
	count = 0;
    vec = new WEIGHT_TYPE[size];
    for (int i = 0; i < size;i++)
        vec[i] = ((WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1)) * 2;
    len = -1;
}
void GVocab::readWord(char* word, FILE *fin) {
    int a = 0, ch;
    ch = fgetc(fin);
    while (ch != -1) {
        //if (ch == 13) continue;
        if ((ch == ' ') || (ch == '\t') || (ch == '\n')) {
            if (a > 0) {
                if (ch == '\n') ungetc(ch, fin);
                break;
            } else
            if (ch == '\n') {
                strcpy(word, (char *)"</s>");
	//printf("readWord :_%s\n", word); //debug
   				return;
            } else {
				ch = fgetc(fin);
				continue;
			}
        }
        word[a] = ch;
        a++;
        if (a >= MAX_WORD_LENGTH - 1) { //Truncate too long words
			word[MAX_WORD_LENGTH - 1] = 0;
			gwarn("GVocab::readWord Meet a very long word : %s\n", word);
			a--;   		
		}
        ch = fgetc(fin);
    }
    word[a] = 0;
	//printf("readWord :_%s\n", word); //debug
}
int GVocab::learnVocabFromFile(const char *fn) {
    addWord("</s>");
    addWord("<unk>");
    FILE *file = fopen(fn, "r");
    if (file == NULL) {
        gerr("Train text file %s not found.\n", fn);
    }
    int wordcn = 0;
    int sentences = 0;
    char buffer[MAX_WORD_LENGTH];
    readWord(buffer, file);
    while (strlen(buffer) != 0) {
        if (!strcmp(buffer, "</s>")) sentences++; else wordcn++;	
		addWord(buffer);
		//words->at(findWord(buffer))->count++; //The counts will be handled by countWordFromFile() 
		readWord(buffer, file);
    }
    fclose(file);
    glog(1, "GVocab::learnVocabFromFile Learned Vocab from file %s, wordcn : %d, sentences : %d, Vocab size now : %d\n", fn, wordcn, sentences, this->size());
    return wordcn;
}
void GVocab::countVocabFromFile(const char *fn) {
    FILE *file = fopen(fn, "r");
    if (file == NULL) {
        gerr("Count-vocab text file %s not found.\n", fn);
    }
    int wordcn = 0;
    int sentences = 0;
    char buffer[MAX_WORD_LENGTH];
    for (int i = 0;i < words->size();i++)
        words->at(i)->count = 0;
    readWord(buffer, file);
    while (strlen(buffer) != 0) {
        if (!strcmp(buffer, "</s>")) sentences++; else wordcn++;	
	    int idx = findWord(buffer);	
        if (idx == -1)
            idx = findWord("<unk>");
        words->at(idx)->count++;
		readWord(buffer, file);
    }
    fclose(file);
    glog(1, "GVocab::countVocabFromFile Counted vocab from file %s, wordcn : %d, sentences : %d, Vocab size now : %d\n", fn, wordcn, sentences, this->size());
}
GVocab::GVocab(int vs, int n) {
    vec_size = vs;
	class_n = n;
	classBegin = new int[class_n + 1];
	for (int i = 0;i < class_n + 1;i++)
		classBegin[i] = -1;
	classLen = new int[class_n + 1]; //class 0 is overall class part
    words = new std::vector<GWord*>();
    hash_table = new int[VOCAB_HASH_SIZE];
    for (int i = 0;i < VOCAB_HASH_SIZE;i++)
        hash_table[i] = -1;
}
int GVocab::getHash(const char *w) {
    long long hash = 0;
    for (int i = 0;i < strlen(w);i++) {
        hash = (hash * 54059 + w[i] + 256) % VOCAB_HASH_SIZE; //w[i] could be negative
    }
    return (int)hash;
}
int GVocab::getUnkInd() {
	int k = findWord("<unk>");
	if (k < 0)
		gerr("GVocab::getUnkInd did not find <unk> in the vocab");
	return k;
}
void GVocab::printVocab(const char *f) {
    FILE *file = fopen(f, "w");
    for (int i = 0;i < words->size();i++)
        fprintf(file, "%s %d\n", words->at(i)->word, words->at(i)->count);
    fclose(file);
}
int GVocab::findWord(const char *w) {  
    int h, beg;
    h = beg = getHash(w);
    while (hash_table[h] != -1) {
        if (!strcmp(words->at(hash_table[h])->word, w)) return hash_table[h];
        h = (h + 1) % VOCAB_HASH_SIZE;
        if (h == beg) 
            break;
    }
    return -1;
}
void GVocab::addWordToHash(const char *w, int index) {
    int h = getHash(w);
    while (hash_table[h] != -1) {
        if (hash_table[h] == index)
            return;
        h = (h + 1) % VOCAB_HASH_SIZE;
    }
    hash_table[h] = index;
}
int GVocab::addWord(const char *w) {
	int k = findWord(w);
	if (k != -1) {
        return k;
    }
    words->push_back(new GWord(w, vec_size));
    addWordToHash(w, words->size() - 1);
	return words->size() - 1;
}
int GVocab::addWV(const char *w, const WEIGHT_TYPE *v, int cou, int cla) {
	int k = findWord(w);
	if (k == -1) 
		k = addWord(w);
	for (int i = 0;i < vec_size;i++)
		words->at(k)->vec[i] = v[i];
	words->at(k)->cla = cla;
    words->at(k)->count = cou;
	return k;
}
int GVocab::getWVFromFile(FILE *file) {
	int lines, size, cn, cla, cou;
	char buffer[MAX_WORD_LENGTH];
	WEIGHT_TYPE buffer_v[vec_size];
	fscanf(file, "%s %d %d %d", buffer, &lines, &cn, &size); //VOCAB vocab_size class_number hidden_size
	if (cn != class_n)
		gerr("GVocab::getWVFromFile class number(%d) mismatch.\n", class_n);
	if (size != vec_size) 
		gerr("GVocab::getWVFromFile vec_size(%d) mismatch.\n", size);
	for (int i = 0;i < lines;i++) {
		fscanf(file, "%d %d", &cla, &cou);
        fscanf(file, "%s", buffer);
		for (int j = 0;j < vec_size;j++)
			fscanf(file, WEIGHTSIGN, buffer_v + j);
		addWV(buffer, buffer_v, cou, cla);
	}
	return words->size();
}
void GVocab::shuffle() {
	glog(1, "GVocab::shuffle shuffling vocab...\n");
	random_shuffle(words->begin(), words->end());
	rehash();
}
void GVocab::sortByCount() {
	glog(1, "GVocab::sortByCount sorting vocab...\n");
	GWord *temp;
	for (int i = 0;i < words->size() - 1;i++)
		for (int j = i + 1;j < words->size();j++)
			if (words->at(i)->count < words->at(j)->count) {
				temp = (*words)[i]; (*words)[i] = (*words)[j]; (*words)[j] = temp;
			}
	rehash();
}
void GVocab::rehash() { 
	glog(1, "GVocab::rehash re-arranging the hash table completely...\n");
    for (int i = 0;i < VOCAB_HASH_SIZE;i++)
        hash_table[i] = -1;
    for (int i = 0;i < words->size();i++)
        addWordToHash(words->at(i)->word, i);
}
int GVocab::size() {
	return words->size();
}
void GVocab::loadVocabFile(const char *file, bool add_su) {
	glog(1, "GVocab::loadVocabFile loading vocab file from %s...\n", file);
	FILE *fin = fopen(file, "r");
    char buffer[MAX_WORD_LENGTH];
    if (add_su) {
        addWord("</s>");
	    addWord("<unk>");
    }
    readWord(buffer, fin);
    while (strlen(buffer) != 0) {
        if (strcmp(buffer, "</s>")) {
            addWord(buffer);
        }
        readWord(buffer, fin);
    }
    fclose(fin);
	glog(1, "GVocab::loadVocabFile loading vocab complete, vocab size now: %d\n", size());
}
int GVocab::getVecSize() {
    return vec_size;
}
void GVocab::assignClass() {
	glog(1, "GVocab::assignClass class number is %d, size of vocab : %d, distributing words to classes...\n", class_n, words->size());
	for (int i = 0;i <= class_n;i++)
		classLen[i] = 0;
	classLen[0] = class_n;
	classBegin[0] = words->size();
    
    int sumFreq = 0, warned = 0;
    for (int i = 0;i < words->size();i++) {
        sumFreq += words->at(i)->count + 1; //+1 to prevent the count is zero
        if (i > 0 && words->at(i)->count > words->at(i - 1)->count && warned == 0) {
            warned = 1;
            gwarn("GVocab::assignClass words(%d)'s count is smaller than words(%d)'s count, supress other warnings...\n", i, i - 1);
        }
    }
    classLen[1] = 0; int k = 1, countNow = 0; 

    for (int i = 0;i < words->size();i++) {
        /* old implementation   
            int k = i / (words->size() / class_n) + 1;
           -        k = k > class_n ? class_n : k;
           -               if (k != words->at(i)->cla && words->at(i)->cla != -1)
               -                       gerr("GVocab::assignClass class conflicts detected %d != %d.\n", k, words->at(i)->cla);
           -               words->at(i)->cla = k;
        */
    	words->at(i)->cla = k;
		classLen[k]++;
	    countNow += words->at(i)->count + 1;
        if (k != words->at(i)->cla && words->at(i)->cla != -1)
			gerr("GVocab::assignClass class conflicts detected %d != %d.\n", k, words->at(i)->cla);
	    if (classBegin[k] == -1)
			classBegin[k] = i;
        if (countNow > (sumFreq * 1.0 / class_n * k)) {
            k = (k >= class_n) ? class_n : (k + 1);
        }
    }

    int minC = words->size(), maxC = 0, minI, maxI;
    for (int i = 1;i <= class_n;i++) {
        if (classLen[i] < minC) {
            minC = classLen[i];
            minI = i;
        }
        if (classLen[i] > maxC) {
            maxC = classLen[i];
            maxI = i;
        }
    }
    glog(1, "GVocab::assignClass assignment complete, smallestClassLength(c%d) : %d biggestClassLength(c%d) : %d\n", minI, minC, maxI, maxC);
}
int GVocab::cn() {
	return class_n;
}
int GVocab::getCL(int c) {
	return classLen[c];
}
int GVocab::getCB(int c) {
	return classBegin[c];
}
GWord *GVocab::getWord(int index) {
	if (index < 0 || index >= size()) {
		gerr("GVocab::getWord Index %d out of range.\n", index);
	}
	return words->at(index);
}	
void GVocab::tieToMatrix(WEIGHT_TYPE *mat) {
    glog(1, "GVocab::tieToMatrix tie to matrix(discarding native word vectors)...\n");
    for (int i = 0;i < words->size();i++) {
        if (strcmp(words->at(i)->word, "</s>") == 0) {
            glog(1, "GVocab::tieToMatrix </s> will not be tied\n");
            continue;
        }
        words->at(i)->vec = mat + vec_size * i;
    }
}

