#include "GVocab.h"
#include "GOpVec.h"
#include "GLib.h"
#include "XNFeed.h"
#include "XRnn.h"
#include "mkl.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>    
#include <vector>
void someTests();

int opPos(char* str, int argc, char** argv) {
    for (int i = 1;i < argc;i++)
        if (!strcmp(str, argv[i]))
            return i;
    return -1;
}

int main(int argc, char** argv) {
    //someTests();
    debug_level = 1; //will be reset
	char buffer[200];
    glog(1, "the X Recurrent Neural Network language model toolkit v0.8 : XRNN_train\n");
    glog(1, "WEIGHT_TYPE : %s\n", WEIGHT_TYPE_STR);

	GOpVec *ops = new GOpVec();
    ops->insert(new GOp("num_threads", "int", "The number of threads INTEL_MKL will occupy.", "10"));
    ops->insert(new GOp("train_file", "file", "Set the training text file, we treat line feed as sentence delimiter, no <s> and </s> should appear. If no -read_vocab is set, the vocab will be constructed from this file.", NULL, false));
    ops->insert(new GOp("valid_file", "file", "Set the valid text file, same as train_file.", NULL, false));
	ops->insert(new GOp("read_lm_file", "file", "If set, xrnn(including gvocab) will be initialized by the file.", NULL, false));
	ops->insert(new GOp("output_lm_file", "file", "Model will be printed to output_lm_file after training.", NULL, false));
	ops->insert(new GOp("valid_first", "int", "Give a valid_file log probability in the first place.", "0"));
	ops->insert(new GOp("iter_number", "int", "The index of current(next) iteration.", "0"));
    ops->insert(new GOp("limit_vocab", "file", "Limit the vocab, otherwise vocab will be learned from the train_file.", NULL, false));
    ops->insert(new GOp("count_vocab", "file", "Set count the vocab from this text file, otherwise counts will be from read_lm_file, or just zero.", NULL, false));
	ops->insert(new GOp("sort_vocab", "int", "if set to 1 and vocab is learned from training file, they will be sorted descendingly by counts.", "0"));
    ops->insert(new GOp("class", "int", "How many classes that the words are divided into.", "1"));
    ops->insert(new GOp("tie", "int", "Whether to tie the word vector and the transformation matrix(log-bilinear model), wvocab tied with weightHO.", "0"));
    ops->insert(new GOp("nnlm", "bool", "Whether using NNLM mode, there will be no recurrent links, will use (bptt + 1) words to predict the current word.", "0"));
    ops->insert(new GOp("alpha", "float", "The learning rate, won't be normalized.", "0.1"));
	ops->insert(new GOp("beta", "float", "The L2 regularization term.", "1e-6"));
    ops->insert(new GOp("rand_seed", "int", "The random seed.", "1"));
    ops->insert(new GOp("debug_level", "int", "Range from 1(most important), 2(running status), 3(diagnostic check added) 4(more info).", "2"));
	ops->insert(new GOp("debug_pause", "int", "Only for debug, pause for n micro-seconds at the end of each level >= 3 glog call and each gvprint call, about 100000 is okay.", "0"));
	ops->insert(new GOp("warn_pause", "int", "The microseconds paused for each gwarn call, about 100000 is okay.", "500000"));
	ops->insert(new GOp("independent", "int", "Whether to use independent sentence mode, 0 or 1, if set to 0, XNFeed will read batch_size * 10 consecutive sentences at one time.", "1"));
    ops->insert(new GOp("print_vocab", "file", "File to print the vocab.", NULL, false)); //could be NULL
    ops->insert(new GOp("hidden_size", "int", "Hidden layer size, which is also the word vector size.", "100"));
    ops->insert(new GOp("batch_size", "int", "Minibatch size(update after how many words).", "10"));
	ops->insert(new GOp("bptt", "int", "How many times to propagate the error back through time.", "3"));
	ops->insert(new GOp("unk_div", "int", "The prob of <unk>(including oov) will be divided by unk_div during testing.", "1"));
 	ops->insert(new GOp("weight_constrain", "int", "Constrain the weights.", "5"));
	 
    std::vector<GOp*>::iterator it;

    if (argc == 1) {
        for (it = ops->begin(); it < ops->end(); it++)
            glog(1, "-%s <%s> %s Default %s.\n", (*it)->name, (*it)->ty, (*it)->str, (*it)->def);
        return 0;
    }

    ops->sweepArgs(argc, argv);

	debug_level = atoi(ops->findOp("debug_level"));
	debug_pause = atoi(ops->findOp("debug_pause"));
	warn_pause = atoi(ops->findOp("warn_pause"));

    for (it = ops->begin(); it < ops->end(); it++)
        glog(1, "-%s=%s %s\n", (*it)->name, (*it)->get, (*it)->how);
	glog(1, "-FEED_CONSECUTIVE_SENTENCE=%d (pre-set-constants)\n", FEED_CONSECUTIVE_SENTENCE);

    srand(atoi(ops->findOp("rand_seed")));
    mkl_set_dynamic(0); //set threads
    mkl_set_num_threads(atoi(ops->findOp("num_threads"))); 

    GVocab *gvocab = new GVocab(atoi(ops->findOp("hidden_size")), atoi(ops->findOp("class")));
    XRnn *xrnn = new XRnn(ops, gvocab);
	//learn vocab from lm_file, limit_vocab, or train_file
	if (ops->findOp("read_lm_file") != NULL) 
		xrnn->initializeByFile();
	else {
		if (ops->findOp("limit_vocab") != NULL)
    	    gvocab->loadVocabFile(ops->findOp("limit_vocab"));
	    else {
    	    gvocab->learnVocabFromFile(ops->findOp("train_file"));
			if (atoi(ops->findOp("class")) > 1) {
				if (atoi(ops->findOp("sort_vocab")) == 1) {
					gvocab->sortByCount();
				} else {
					glog(1, "main class number > 1, shuffle vocab to prevent possible training scre.\n");
					gvocab->shuffle();
				}
			}
    	}
        xrnn->initialize(); //vocab will also be assign class here
	}
	
    if (ops->findOp("print_vocab") != NULL) {
        gvocab->printVocab(ops->findOp("print_vocab"));
    }

	if (atoi(ops->findOp("valid_first")) == 1) {
		gbiglog(1, "INITIAL CROSSVAL %s", ggettime(buffer));
		xrnn->propagateOnFile(ops->findOp("valid_file"));	
		gbiglog(1, "INITIAL CROSSVAL ENDS %s", ggettime(buffer));
	}

	if (ops->findOp("train_file") != NULL) {
		gbiglog(1, "ITERATION : %d %s", atoi(ops->findOp("iter_number")), ggettime(buffer));
		xrnn->trainOneIteration();
		gbiglog(1, "ITERATION : %d ENDS %s", atoi(ops->findOp("iter_number")), ggettime(buffer));
		
		gbiglog(1, "CROSSVAL ITERATION : %d %s", atoi(ops->findOp("iter_number")), ggettime(buffer));
		xrnn->propagateOnFile(ops->findOp("valid_file"));	
		gbiglog(1, "CROSSVAL ITERATION : %d ENDS %s", atoi(ops->findOp("iter_number")), ggettime(buffer));
	}

	if (ops->findOp("output_lm_file") != NULL) {
		xrnn->printModel();
	}

	return 0;
}
void someTests() { //An exit(0) is added in the end
    double b[] = {1, 2, 3, -4, 5, 6};
    double av[] = {1, 1, 1}; 
    int m = 3, n = 3, k = 2;
    int ac[] = {0, 0, 2};
    int ab[] = {0, 2, 2};
    int ae[] = {2, 2, 3};
    double alpha = 1, beta = 0;
    double c[9];
    mkl_dcsrmm("T", &m, &n, &k, &alpha, "GLUC", av, ac, ab, ae, b, &n, &beta, c, &n); //Zero::row-major
    for (int i = 0;i < 3;i++) {
        for (int j = 0;j < 3;j++) {
            printf("%lf ", c[i * 3 + j]);
        }
        printf("\n");
    }
    // Codes for testing glib_splitWordUTF8
    //glib_splitWordUTF8("Ah我是abc.。你是？");
    
	/* Codes for testing XNFeed
	int aa[batch_size];
	while (feed->nextBatch(aa)) {
		for (int i = 0;i < batch_size;i++)
			if (aa[i] == -1) 
				printf("<zero> ");
		 	else
				printf("%8s ", gvocab->getWord(aa[i])->word);
		printf("\n");
	}
	*/
    
    /*//Codes for testing CBLAS_AXPY	
    WEIGHT_TYPE aa[10];
    for (int i = 0;i < 10;i++)
        aa[i] = 1;
    CBLAS_AXPY(10, -0.1, aa, 1, aa, 1);
    for (int i = 0;i < 10;i++)
        printf("%lf ", aa[i]);
    printf("\n");
    */
    
	/* Codes for testing glib_vsigmoid
	WEIGHT_TYPE *aa = new WEIGHT_TYPE[10];
	WEIGHT_TYPE *oo = new WEIGHT_TYPE[10];
	for (int i = 0;i < 10;i++) {
		aa[i] = 0.1 * i;
		oo[i] = 1;
	}
	glib_vsigmoid(10, aa, aa, oo);
	for (int i = 0;i < 10;i++)
		printf("%f ", aa[i]);
	printf("\n");
	*/

	/*
	//Codes for testing glib_vsoftmax
	int tn = 3;
	WEIGHT_TYPE *aa = new WEIGHT_TYPE[tn];
	for (int i = 0;i < tn;i++) {
		aa[i] = i;
	}
	glib_vsoftmax(tn, aa, aa);
	for (int i = 0;i < tn;i++)
		printf("%f ", aa[i]);
	printf("\n");
	*/

	/*
	//Codes for testing glib_vsigerr
	int tn = 3;
	WEIGHT_TYPE *aa = new WEIGHT_TYPE[tn];
	WEIGHT_TYPE *bb = new WEIGHT_TYPE[tn];
	WEIGHT_TYPE *oo = new WEIGHT_TYPE[tn];
	for (int i = 0;i < tn;i++) {
		aa[i] = 0.1 * i;
		oo[i] = 1;
	}
	glib_vsigerr(tn, aa, bb, oo);
	for (int i = 0;i < tn;i++)
		printf("%f ", bb[i]);
	printf("\n");
	*/

	exit(0);
}
