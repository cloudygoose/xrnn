#include "XRnn.h"
#include "stdlib.h"
#include <string.h>
#include "mkl.h"
#include <math.h>
#include "unistd.h"
#include <time.h>
#include <ctime>
#include <cstdio>

TestRes::TestRes(double d1, double d2, int wc, int sc, int oc) {
	logp_net = d1; logp_net_oovpart = d2;
	logp_other = 0; logp_other_oovpart = 0;
	logp_combine = 0; logp_combine_oovpart = 0;
	wordcn = wc; sencn = sc; oovcn = oc;
}
TestRes::TestRes(double d1, double d2, double d3, double d4, double d5, double d6, int wc, int sc, int oc) {
	logp_net = d1; logp_net_oovpart = d2;
	logp_other = d3; logp_other_oovpart = d4;
	logp_combine = d5; logp_combine_oovpart = d6;
	wordcn = wc; sencn = sc; oovcn = oc;
}
double TestRes::giveNetPPL() {
	return exp10((-(logp_net - logp_net_oovpart)) / (wordcn - oovcn + sencn));
}
double TestRes::giveNetPPLwithOOV() {
	return exp10(-(logp_net) / (wordcn + sencn));
}
double TestRes::giveOtherPPL() {
	return exp10((-(logp_other - logp_other_oovpart)) / (wordcn - oovcn + sencn));
}
double TestRes::giveOtherPPLwithOOV() {
	return exp10(-(logp_other) / (wordcn + sencn));
}
double TestRes::giveCombinePPL() {
	return exp10((-(logp_combine - logp_combine_oovpart)) / (wordcn - oovcn + sencn));
}
double TestRes::giveCombinePPLwithOOV() {
	return exp10(-(logp_combine) / (wordcn + sencn));
}
XRnn::XRnn(GOpVec *ops, GVocab *voc) {
	vocab = voc;
    tie = false;
	log_number = 10000;
	batch_size = atoi(ops->findOp("batch_size"));
	hidden_size = atoi(ops->findOp("hidden_size"));
	output_size = -1; //Changed in initialization.
	glog(2, "XRnn::XRnn </s>'s id in vocab is %d\n", senId);
	bptt = atoi(ops->findOp("bptt"));
	alpha = atof(ops->findOp("alpha"));
	beta = atof(ops->findOp("beta"));
    unk_div = atoi(ops->findOp("unk_div"));
	independent = atoi(ops->findOp("independent"));
	weightConstrain = atof(ops->findOp("weight_constrain"));
    profiler = new XProfiler();
	initialized = false;
    if (atoi(ops->findOp("tie")) == 1)
        tie = true;
    nnlm = false;
    if (atoi(ops->findOp("nnlm")) == 1)
        nnlm = true;
	opVec = ops;
}
void XRnn::makeSpace() {
	if (initialized) {
		gerr("XRnn::makeSpace already initialized.\n");
	}
	
	printf("XRnn::makeSpace class_number : %d hidden_size : %d output_size : %d batch_size : %d\n", vocab->cn(), hidden_size, output_size, batch_size);
	
	trainClassCount = new int[vocab->cn() + 1];
	for (int i = 0;i < vocab->cn() + 1;i++)
		trainClassCount[i] = 0;

    int vhLen = 1;
    if (nnlm)
        vhLen = bptt + 1;
    weightVH = new WEIGHT_TYPE*[vhLen + 1];
    weightVH_back = new WEIGHT_TYPE*[vhLen + 1];
    for (int i = 0;i < vhLen;i++) {
        weightVH[i] = new WEIGHT_TYPE[hidden_size * hidden_size];
        weightVH_back[i] = new WEIGHT_TYPE[hidden_size * hidden_size];
    }

	weightHO = new WEIGHT_TYPE[hidden_size * output_size];
    //biasO = new WEIGHT_TYPE[output_size]; biasO is removed since no gain is observed
    if (!nnlm) {
        weightHH = new WEIGHT_TYPE[hidden_size * hidden_size];
	    weightHH_back = new WEIGHT_TYPE[hidden_size * hidden_size];
    }

	hiddenHis = new WEIGHT_TYPE[(bptt + 2) * hidden_size * batch_size];
	batchNow = new int[batch_size];
	batchHis = new int[(bptt + 2) * batch_size];
    for (int i = 0;i < (bptt + 2) * batch_size;i++)
        batchHis[i] = -1;

	outputV = (WEIGHT_TYPE**)malloc(sizeof(WEIGHT_TYPE*) * (vocab->cn() + 1));
	for (int i = 0;i < vocab->cn() + 1;i++) {
		if (vocab->getCL(i) <= 0)
			gerr("XRnn::makeSpace length is invalid for class %d\n", vocab->getCL(i));
		outputV[i] = new WEIGHT_TYPE[vocab->getCL(i) * batch_size];
	}
	hiddenCache = (WEIGHT_TYPE**)malloc(sizeof(WEIGHT_TYPE*) * (vocab->cn() + 1)); //class is indexed from 1 to vocab->cn()
	for (int i = 0;i < vocab->cn() + 1;i++) {
		hiddenCache[i] = new WEIGHT_TYPE[hidden_size * batch_size * (bptt + 2)];
		SCAL(hidden_size * batch_size * (bptt + 2), 0, hiddenCache[i], 1); 
	}
	batchCache =  (int**)malloc(sizeof(int*) * (vocab->cn() + 1));
	for (int i = 0;i < vocab->cn() + 1;i++) {
		batchCache[i] = new int[batch_size * (bptt + 2)];
	}
	cacheCount = new int[vocab->cn() + 1];
	for (int i = 0;i < vocab->cn() + 1;i++)
		cacheCount[i] = 0;	

	hiddenError = new WEIGHT_TYPE[batch_size * hidden_size];
	
	int len = hidden_size; //Initialize vector of one
	if (output_size > hidden_size)
		len = output_size;
	len = len * batch_size;
	oneV = new WEIGHT_TYPE[len];
	for (int i = 0;i < len;i++)
		oneV[i] = 1;
	auxV = new WEIGHT_TYPE[len];
	for (int i = 0; i < len;i++)
		auxV[i] = 0;
    auxV2 = new WEIGHT_TYPE[len];
}
int XRnn::checkProb(double *prob, char *word) {
	if (*prob < TOO_SMALL_PROB) {
		*prob = TOO_SMALL_PROB;
		if (debug_level >= 2)
			gwarn("XRnn::checkProb Meets a probability for \"%s\" too small(%lf, changed).\n", *prob, word);
		return 1;
	}
	if (isnan(*prob)) { //From math.h
		gerr("XRnn::checkProb Meets a nan probability, exiting...\n");
	}
	return 0;
}
void XRnn::constrainWeights(int len, WEIGHT_TYPE *m) {
	for (int i = 0;i < len;i++) {
		if (m[i] > weightConstrain) 
			m[i] = weightConstrain * 0.9;
		if (m[i] < -weightConstrain)
			m[i] = -weightConstrain * 0.9;
	}
}
void XRnn::debugMatrix(const char *name, int len, WEIGHT_TYPE *m) {
	if (debug_level < 3)
		return;
	for (int i = 0;i < len;i++)
		if (isnan(m[i])) {
			char buffer[200];
			sprintf(buffer, "XRnn::debugMatrix position %d at %s is nan.\n", i, name);
			gerr(buffer);
		}
}
void XRnn::checkProbV(int len, WEIGHT_TYPE *pv) {
	if (debug_level < 3)
		return;
	WEIGHT_TYPE sum = 0;
	for (int i = 0;i < len;i++) {
		if (isnan(pv[i])) {	
			gerr("XRnn::checkProbV Meets a nan prob.\n");
		}
		else
			if (pv[i] < 0)
				gerr("XRnn::checkProbV Meets a negative prob.\n");
		sum += pv[i];
	}
	if (sum < 0.999 || sum > 1.001)
		gerr("XRnn::checkProbV The probV sum is not one.\n");
}		
void XRnn::initialize() {
	if (initialized) {
		gerr("XRnn::initialize model already initialized.\n");
	}
    glog(1, "XRnn::intialize dealing with vocab...\n");
    if (opVec->findOp("count_vocab") != NULL)
        vocab->countVocabFromFile(opVec->findOp("count_vocab"));

    if (atoi(opVec->findOp("sort_vocab")) == 1)
        vocab->sortByCount();
	vocab->assignClass();

    glog(1, "XRnn::initialize initialize model randomly...\n");
	senId = vocab->findWord("</s>");
	unkId = vocab->findWord("<unk>");
	
	if (senId == -1 || unkId == -1)
		gerr("XRnn::XRnn </s> or <unk> not found in vocab");

    output_size = vocab->size() + vocab->cn();
    makeSpace();
		
	for (int i = 0;i < hidden_size * output_size;i++) 
		weightHO[i] = ((WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1)) * 2; 
   
    if (!nnlm)
    	for (int i = 0;i < hidden_size * hidden_size;i++)
	    	weightHH[i] = ((WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1)) * 2;

    int vhLen = nnlm ? bptt + 1 : 1;
    for (int l = 0;l < vhLen;l++) {
        //if tie==0 && !nnlm, weightVH would be of no help
        for (int i = 0;i < hidden_size * hidden_size;i++)
            if (tie == 0 && !nnlm)
                weightVH[l][i] = 0; 
            else
                weightVH[l][i] = ((WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1)) * 2;
        if (tie == 0 && !nnlm) { //initialize it to identity matrix
            for (int i = 0;i < hidden_size;i++)
                weightVH[l][i * hidden_size + i] = 1;
        }
    }

    /* biasO is removed since no gain is observed
    for (int i = 0;i < output_size;i++)
 		biasO[i] = ((WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1) + (WEIGHT_TYPE)grand(-0.1, 0.1)) * 2;
    */

    if (tie) {
        vocab->tieToMatrix(weightHO); //Tie word vector
   }

	initialized = true;
}
void XRnn::initializeByFile() {
	if (initialized) 
		gerr("XRnn::initializeByFile Model already initialized.\n");
	if (opVec->findOp("read_lm_file") == NULL) 
		gerr("XRnn::initializeByFile read_lm_file option is NULL.\n");

	glog(1, "XRnn::initializeByFile begin reading lm from file %s...\n", opVec->findOp("read_lm_file"));
	
	FILE *file = fopen(opVec->findOp("read_lm_file"), "r");

	int a, b, c;
	char buffer[100];

	//read vocab
	vocab->getWVFromFile(file);
    if (opVec->findOp("count_vocab") != NULL) {
        vocab->countVocabFromFile(opVec->findOp("count_vocab"));
    }
    //vocab->sortByCount();
	vocab->assignClass();
	output_size = vocab->size() + vocab->cn(); //important

	makeSpace();

    int vhLen = nnlm ? bptt + 1 : 1;
    for (int l = 0;l < vhLen;l++) {
        fscanf(file, "%s %d %d %d", buffer, &c, &a, &b);
        if (a != hidden_size || b != hidden_size || strcmp(buffer, "WEIGHTVH") != 0 || c != l)
            gerr("XRnn::initializeByFile weightVH(%d %d) meta mismatch.\n", hidden_size, hidden_size);
        for (int i = 0;i < hidden_size;i++)
            for (int j = 0;j < hidden_size;j++)
                fscanf(file, WEIGHTSIGN, weightVH[l] + hidden_size * i + j);
    }

	if (!nnlm) {
        fscanf(file, "%s %d %d", buffer, &a, &b);
        if (a != hidden_size || b != hidden_size)
            gerr("XRnn::initializeByFile weightHH(%d %d) size mismatch.\n", hidden_size, hidden_size);
        for (int i = 0;i < hidden_size;i++) 
            for (int j = 0;j < hidden_size;j++) 
                fscanf(file, WEIGHTSIGN, weightHH + hidden_size * i + j);
    }

	fscanf(file, "%s %d %d", buffer, &a, &b);
	if (a != hidden_size || b != output_size)
		gerr("XRnn::initializeByFile weightHO(%d %d) size mismatch.\n", hidden_size, output_size);
	for (int i = 0;i < output_size;i++) 
		for (int j = 0;j < hidden_size;j++)
			fscanf(file, WEIGHTSIGN, weightHO + hidden_size * i + j);
  
    /* biasO is removed since no gain is observed
    fscanf(file, "%s %d", buffer, &a);
    if (a != output_size)
        gerr("XRnn::initializeByFile biasO(%d) size mismatch.\n", output_size);
    for (int i = 0;i < output_size;i++)
        fscanf(file, WEIGHTSIGN, biasO + i);
    */
	fclose(file);
	glog(1, "XRnn::initializeByFile reading lm from file complete.\n");

	senId = vocab->findWord("</s>");
	unkId = vocab->findWord("<unk>");
	
	if (senId == -1 || unkId == -1)
		gerr("XRnn::XRnn </s> or <unk> not found in vocab");

    if (tie) {
        vocab->tieToMatrix(weightHO); //Tie word vector
   }

	initialized = 1;
}
TestRes* XRnn::propagateOnFile(const char *fn) {
	if (fn == NULL) {
		gerr("XRnn::propagateOnFile fn NULL");
	}
	glog(1, "XRnn::propagateOnFile propogating on file %s...\n", fn);
	if (independent == 0 || opVec->findOp("clean_out_file") != NULL) {
		glog(1, "XRnn::propagateOnFile dependent mode or outputing n-best format file, batch_size set to 1...\n");
		batch_size = 1;
	}
	glog(1, "XRnn::propagateOnFile prob for <unk> will be divided by %d...\n", unk_div);
	FILE *nbest_file = NULL;
	if (opVec->findOp("clean_out_file") != NULL) {
		glog(1, "XRnn::propagateOnFile Will print n-best format output to %s...\n", opVec->findOp("clean_out_file"));
		nbest_file = fopen(opVec->findOp("clean_out_file"), "w");
	}

	XNFeed *feeder = new XNFeed(batch_size, independent, vocab);
	feeder->initialize(fn);

	double logp = 0, logp_oovpart = 0; //Summation of log probability
	double pnow;
	int wordcn = 0; //Count of words	
	int sencn = 0;	
	int oovcn = 0;
	int batch_id = 0; //Batch_id is used to calculate the position of some batch in history 
	int tooSmallProb = 0;

	double senlogp = 0; //Summation of a sentence of log p
	posNow = 0;
	posLast = -1;
	while (feeder->nextBatch(batchNow)) {
		if (posLast != -1) { //outputV now saves last batch's outputs
			propagateHO(hiddenHis + hidden_size * batch_size * posLast, 0);
			//gvprint("outputV[0]", batch_size * vocab->cn(), outputV[0]);
			for (int i = 0;i < batch_size;i++) {
				if (batchNow[i] != -1) {
					int cla = vocab->getWord(batchNow[i])->cla;
					if (cla == 0 || cla > vocab->cn())
						gerr("XRnn::propagateOnFile meet an invalid class %d for word %d.\n", cla, batchNow[i]);
					//checkProbV(vocab->cn(), outputV[0] + vocab->cn() * i); //Check the probability is okay
					propagateHO(hiddenHis + hidden_size * batch_size * posLast, cla); //A lot of redundant calculation here when batch_size != 1
					//checkProbV(vocab->getCL(cla), outputV[cla] + vocab->getCL(cla) * i); 
					
					pnow = getProb(i, batchNow[i]);
					glog(4, "%s:%.6f  ", vocab->getWord(batchNow[i])->word, pnow);
					tooSmallProb += checkProb(&pnow, vocab->getWord(batchNow[i])->word);
					if (batchNow[i] == unkId) {
						oovcn++;
						pnow /= unk_div;
						logp_oovpart += log10(pnow);
					}
					logp += log10(pnow);		

					if (batchNow[i] == senId) {
						sencn++;
					}
					else {
						wordcn++;
					}

					if (nbest_file != NULL) { //output nbest-format probability
						fprintf(nbest_file, "%s %lf ", vocab->getWord(batchNow[i])->word, pnow);
						senlogp += log10(pnow);
						if (batchNow[i] == senId) {
							fprintf(nbest_file, "%lf\n", senlogp);
							senlogp = 0;
						}
					}
				} else //Stream ends
					glog(4, "<NULL> ");	
			}
			glog(4, "logp(summation) : %.6lf", logp);
			glog(4, "\n");
		}		
			
		/** Propagation **/
		for (int i = 0;i < batch_size;i++) //Move the current batch to batchHis
			batchHis[batch_size * posNow + i] = batchNow[i];
        if (!nnlm)
    		propagateHH(posLast, posNow, hiddenHis, batchNow);
        else
            propagateVH(posNow, hiddenHis, batchHis);
		//gvprint("hidden_vector", batch_size * hidden_size, hiddenHis + batch_size * hidden_size * posNow);

		if (batch_id * batch_size % log_number - (batch_id - 1) * batch_size % log_number < 0) {
			glog(2, "\\T");
        }

		batch_id++;
		posLast = posNow;
		posNow = (posNow + 1) % (bptt + 2);
	}
	glog(1, "\n");
	glog(1, "XRnn::propagateOnFile propagation complete\n");
	glog(1, "XRnn::propagateOnFile number-of-too-small-probs: %d\n", tooSmallProb); 
	glog(1, "XRnn::propagateOnFile wordcn: %d sencn: %d oovcn: %d\n", wordcn, sencn, oovcn);
	glog(1, "XRnn::propagateOnFile logp-for-this-file: %.5lf\n", logp);
	
	batch_size = atoi(opVec->findOp("batch_size"));
	glog(1, "XRnn::propagateOnFile batch_size restored to %d\n", batch_size);
	if (nbest_file != NULL)
		fclose(nbest_file);

	return (new TestRes(logp, logp_oovpart, wordcn, sencn, oovcn));
}
TestRes* XRnn::propagateOnFileInterpolate(const char *fn, double inter_this, const char *otherp_fn) {
	if (fn == NULL || otherp_fn == NULL) {
		gerr("XRnn::propagateOnFileInterpolate fn NULL");
	}
	if (batch_size != 1) 
		gerr("XRnn::propagateOnFileInterpolate batch_size is not one");

	glog(1, "XRnn::propagateOnFileInterpolate propogating on file %s, alone with %s...\n", fn, otherp_fn);

	FILE *file = fopen(otherp_fn, "r");
	if (independent == 0 || opVec->findOp("clean_out_file") != NULL) {
		glog(1, "XRnn::propagateOnFileInterpolate batch_size set to 1...\n");
		batch_size = 1;
	}
    
    FILE *nbest_file = NULL;
	if (opVec->findOp("clean_out_file") != NULL) {
		glog(1, "XRnn::propagateOnFile Will print n-best format output to %s...\n", opVec->findOp("clean_out_file"));
		nbest_file = fopen(opVec->findOp("clean_out_file"), "w");
	}

	glog(1, "XRnn::propagateOnFileInterpolate prob for <unk> will be divided by %d...\n", unk_div);
	
	XNFeed *feeder = new XNFeed(batch_size, independent, vocab);
	feeder->initialize(fn);

	double logp = 0, logp_oovpart = 0, logp_other = 0, logp_other_oovpart = 0, logp_combine = 0, logp_combine_oovpart = 0; //Summation of log probability
	double pnow, pother, pcombine, senlogp = 0;
	int wordcn = 0; //Count of words	
	int sencn = 0;	
	int oovcn = 0;
	int oovcn_other = 0;
	int batch_id = 0; //Batch_id is used to calculate the position of some batch in history 
	int tooSmallProb = 0;
	bool other_unk;
	char buffer[MAX_WORD_LENGTH];

	posNow = 0;
	posLast = -1;
	while (feeder->nextBatch(batchNow)) {
		if (posLast != -1) { //outputV now saves last batch's outputs
			propagateHO(hiddenHis + hidden_size * batch_size * posLast, 0);
			for (int i = 0;i < batch_size;i++) {
			if (batchNow[i] != -1) {
					int cla = vocab->getWord(batchNow[i])->cla;
					propagateHO(hiddenHis + hidden_size * batch_size * posLast, cla); //A lot of redundant calculation here when batch_size != 1
					checkProbV(vocab->cn(), outputV[0] + i * vocab->cn()); //Check the probability is okay
					checkProbV(vocab->getCL(cla), outputV[cla] + i * vocab->getCL(cla)); 
					pnow = getProb(i, batchNow[i]);
					fscanf(file, "%lf %s", &pother, buffer);

					glog(4, "t|%s|o|%s:t%.6fo%.6f ", vocab->getWord(batchNow[i])->word, buffer, pnow, pother);
					tooSmallProb += checkProb(&pnow, vocab->getWord(batchNow[i])->word);
					if (checkProb(&pother, vocab->getWord(batchNow[i])->word) == 1) {
						other_unk = true;
						oovcn_other++;
					} else
						other_unk = false;
                    
                    if (batchNow[i] == unkId)
						pnow /= unk_div;

                    pcombine = inter_this * pnow + (1 - inter_this) * pother;

					if (batchNow[i] == unkId || other_unk) {
						oovcn++;
    					logp_oovpart += log10(pnow);
						logp_other_oovpart += log10(pother);
						logp_combine_oovpart += log10(pcombine);
					}

					logp += log10(pnow);	
					logp_other += log10(pother);
					logp_combine += log10(pcombine);

					if (batchNow[i] == senId) 
						sencn++;
					else 
						wordcn++;
                    
                    if (nbest_file != NULL) { //output nbest-format probability
						fprintf(nbest_file, "%s %lf ", vocab->getWord(batchNow[i])->word, pcombine);
						senlogp += log10(pcombine);
						if (batchNow[i] == senId) {
							fprintf(nbest_file, "%lf\n", senlogp);
							senlogp = 0;
						}
					}
				} else //Stream ends
					glog(4, "<NULL> ");	
			}
			glog(4, "logp(summation) : %.6lf", logp);
			glog(4, "\n");
		}		
			
		/** Propagation **/
		for (int i = 0;i < batch_size;i++) //Move the current batch to batchHis
			batchHis[batch_size * posNow + i] = batchNow[i];
        if (!nnlm)
    		propagateHH(posLast, posNow, hiddenHis, batchNow);
        else
            propagateVH(posNow, hiddenHis, batchHis);
		//gvprint("hidden_vector", batch_size * hidden_size, hiddenHis + batch_size * hidden_size * posNow);

		if (batch_id * batch_size % log_number - (batch_id - 1) * batch_size % log_number < 0)
			glog(2, "\\T");
	
		batch_id++;
		posLast = posNow;
		posNow = (posNow + 1) % (bptt + 2);
	}

	glog(1, "\n");
	glog(1, "XRnn::propagateOnFileInterpolate propagation complete\n");
	glog(1, "XRnn::propagateOnFileInterpolate number-of-too-small-probs-given-by-rnn: %d\n", tooSmallProb); 
	glog(1, "XRnn::propagateOnFileInterpolate wordcn: %d sencn: %d oovcn: %d\n", wordcn, sencn, oovcn);
	glog(1, "XRnn::propagateOnFileInterpolate oovcn-other-lm : %d\n", oovcn_other);
	glog(1, "XRnn::propagateOnFileInterpolate logp-rnn-for-this-file: %.5lf logp-no-oov: %.5lf\n", logp, logp - logp_oovpart);
	glog(1, "XRnn::propagateOnFileInterpolate logp-other-for-this-file: %.5lf logp-other-no-oov %.5lf\n", logp_other, logp_other - logp_other_oovpart);
	glog(1, "XRnn::propagateOnFileInterpolate logp-combine-for-this-file: %.5lf logp-combine-no-oov %.5lf\n", logp_combine, logp_combine - logp_combine_oovpart);

	batch_size = atoi(opVec->findOp("batch_size"));
	glog(1, "XRnn::propagateOnFileInterpolate batch_size restored to %d\n", batch_size);

    if (nbest_file != NULL)
		fclose(nbest_file);

	return (new TestRes(logp, logp_oovpart, logp_other, logp_other_oovpart, logp_combine, logp_combine_oovpart, wordcn, sencn, oovcn));
}
void XRnn::printModel() {
	if (opVec->findOp("output_lm_file") == NULL) {
		gerr("XRnn::printModel output_lm_file is NULL.");
	}

	glog(1, "XRnn::printModel outputing model to %s...\n", opVec->findOp("output_lm_file"));

	FILE *file = fopen(opVec->findOp("output_lm_file"), "w");

	fprintf(file, "VOCAB %d %d %d\n", vocab->size(), vocab->cn(), vocab->getVecSize());
	for (int i = 0;i < vocab->size();i++) {
		fprintf(file, "%d %d %s ", vocab->getWord(i)->cla, vocab->getWord(i)->count, vocab->getWord(i)->word);
		for (int j = 0;j < vocab->getVecSize();j++)
			fprintf(file, "%.6lf ", vocab->getWord(i)->vec[j]);
		fprintf(file, "\n");
	}
    
    int vhLen = nnlm ? bptt + 1 : 1;
    for (int l = 0;l < vhLen;l++) {
        fprintf(file, "WEIGHTVH %d %d %d\n", l, hidden_size, hidden_size);
        for (int i = 0;i < hidden_size;i++) {
            for (int j = 0;j < hidden_size;j++)
                fprintf(file, "%.6lf ", weightVH[l][hidden_size * i + j]);
            fprintf(file, "\n");
        }
    }

    if (!nnlm) {
        fprintf(file, "WEIGHTHH %d %d\n", hidden_size, hidden_size);
        for (int i = 0;i < hidden_size;i++) {
            for (int j = 0;j < hidden_size;j++) 
                fprintf(file, "%.6lf ", weightHH[hidden_size * i + j]);
            fprintf(file, "\n");
        }
    }

	fprintf(file, "WEIGHTHO %d %d\n", hidden_size, output_size);
	for (int i = 0;i < output_size;i++) {
		for (int j = 0;j < hidden_size;j++) {
			fprintf(file, "%.6lf ", weightHO[hidden_size * i + j]);	
		}
		fprintf(file, "\n");
	}

    /* biasO is removed since no gain is observed
    fprintf(file, "BIASO %d\n", output_size);
    for (int i = 0;i < output_size;i++)
        fprintf(file, "%.6lf ", biasO[i]);
    fprintf(file, "\n");
    */

	fclose(file);	
	glog(1, "XRnn::printModel outputing model complete.\n");
}
void XRnn::trainOneIteration() {
	if (opVec->findOp("train_file") == NULL) {
		gerr("XRnn::trainOneIteration train_file option NULL");
	}
	
	glog(1, "XRnn::trainOneIteration propogating for training file %s...\n", opVec->findOp("train_file"));

	XNFeed *feeder = new XNFeed(batch_size, independent, vocab);
	feeder->initialize(opVec->findOp("train_file"));
	double logp = 0; //Summation of log probability
	int wordcn = 0; //Count of words	
	int sencn = 0;

	int batch_id = 0; //How many batches have been processed
	int tooSmallProb = 0; //Count the number of too small prob(returned by checkProb)

	posNow = 0;
	posLast = -1;
	while (feeder->nextBatch(batchNow)) {
		if (posLast != -1) {
			//distributes the hidden activations to the cache
			for (int i = 0;i < batch_size;i++) {
				if (batchNow[i] == -1) //stream ends
					continue;
				int cla = vocab->getWord(batchNow[i])->cla;
				batchCache[cla][cacheCount[cla]] = batchNow[i];
				if (vocab->cn() > 1)
					batchCache[0][cacheCount[0]] = batchNow[i];
				int p = posLast; //begins to store history to the cache, hiddenCache[cla][0->hidden_size] is wasted
				for (int j = 1;j < bptt + 2;j++) {
					if (batch_id - j < 0) {
						batchCache[cla][batch_size * j + cacheCount[cla]] = -1;
						if (vocab->cn() > 1)
							batchCache[0][batch_size * j + cacheCount[0]] = -1;
					}
					else {
						batchCache[cla][batch_size * j + cacheCount[cla]] = batchHis[p * batch_size + i];
						CBLAS_COPY(hidden_size, hiddenHis + p * hidden_size * batch_size + hidden_size * i, 1, hiddenCache[cla] + j * hidden_size * batch_size + hidden_size * cacheCount[cla], 1);
						if (vocab->cn() > 1) {
							batchCache[0][batch_size * j + cacheCount[0]] = batchHis[p * batch_size + i];
							CBLAS_COPY(hidden_size, hiddenHis + p * hidden_size * batch_size + hidden_size * i, 1, hiddenCache[0] + j * hidden_size * batch_size + hidden_size * cacheCount[0], 1);
						}
					}
					p = ((p - 1) < 0 ) ? bptt + 1 : p - 1;
				}
				cacheCount[cla]++;
				if (vocab->cn() > 1)
					cacheCount[0]++;

				if (cacheCount[cla] == batch_size) {//the cache for class cla get full, let's train the model
                    trainClass(cla, logp, tooSmallProb, wordcn, sencn);
					cacheCount[cla] = 0;
					SCAL(hidden_size * batch_size * (bptt + 2), 0, hiddenCache[cla], 1); 
				}

				if (cacheCount[0] == batch_size && vocab->cn() > 1) {			
					trainClass(0, logp, tooSmallProb, wordcn, sencn);
					cacheCount[0] = 0;
					SCAL(hidden_size * batch_size * (bptt + 2), 0, hiddenCache[0], 1); 
				}
			}
		}
		/** Propagation **/
		for (int i = 0;i < batch_size;i++) //Move the current batch to batchHis
			batchHis[batch_size * posNow + i] = batchNow[i];
		if (!nnlm)
            propagateHH(posLast, posNow, hiddenHis, batchNow);
        else {
            propagateVH(posNow, hiddenHis, batchHis); 
        }
		//gvprint("XRnn::trainOneIteration", batch_size * hidden_size, hiddenHis + batch_size * hidden_size * posNow);

		if (batch_id * batch_size % log_number - (batch_id - 1) * batch_size % log_number < 0) {
		    glog(2, "\\T");	//Indicating 10000 words is processed
            profiler->printToLog();
        }
		posLast = posNow;
		posNow = (posNow + 1) % (bptt + 2);
		batch_id++;
	}	
	glog(2, "\n"); //For finishing the 'glog(2, "\\T");'
	glog(1, "XRnn::trainOneIteration finishing training for what's left in the cache...\n");
	for (int i = 0;i <= vocab->cn();i++)
		if (cacheCount[i] > 0) {
			trainClass(i, logp, tooSmallProb, wordcn, sencn);
			cacheCount[i] = 0;
		}

	glog(3, "XRnn::trainOneIteration showing class batches : ");
	for (int i = 0;i <= vocab->cn();i++)
		glog(3, "%d:%d ", i, trainClassCount[i]);
	glog(3, "\n");

	glog(1, "XRnn::trainOneIteration complete\n");
	glog(1, "XRnn::trainOneIteration number-of-too-small-probs: %d\n", tooSmallProb);
	glog(1, "XRnn::trainOneIteration wordcn: %d sencn: %d\n", wordcn, sencn);
	glog(1, "XRnn::trainOneIteration logp-for-train-file: %.5lf\n", logp);	
}
void XRnn::trainClass(int c, double &logp, int &tooSmallProb, int &wordcn, int &sencn) {
	glog(4, "XRnn::trainClass begining training for class %d, wordcn : %d\n", c, wordcn);	
    profiler->start("XRnn::trainClass");
    //gvprint("biasO[0]", vocab->getCL(0), biasO + vocab->getCB(0));
	trainClassCount[c]++;
	double pnow;
	int ba = cacheCount[c];
	double alpha_n = alpha; //I decided not to normalize the alpha so that the user can use 0.1 as beginning alpha no matter how large the batch_size is
    propagateHO(hiddenCache[c] + batch_size * hidden_size, 0); //The first batch is wasted
	if (c != 0) {
		propagateHO(hiddenCache[c] + batch_size * hidden_size, c);
		for (int i = 0;i < ba;i++) { //Handles statistics
			checkProbV(vocab->getCL(0), outputV[0] + i * vocab->getCL(0));
			checkProbV(vocab->getCL(c), outputV[c] + i * vocab->getCL(c)); 

			pnow = getProb(i, batchCache[c][i]);
    		glog(4, "%s:%.7f  ", vocab->getWord(batchCache[c][i])->word, pnow);
			tooSmallProb += checkProb(&pnow, vocab->getWord(batchCache[c][i])->word);
			logp += log10(pnow);		
			if (batchCache[c][i] == senId) 
				sencn++;
			else
				wordcn++;
		}
	}
	glog(4, "\n");
	//Calculate output layer error vector
	SCAL(ba * vocab->getCL(c), -1, outputV[c], 1);
	for (int i = 0;i < ba;i++) {
		if (c == 0) //The batch for class 0 could different classes
			outputV[0][vocab->getWord(batchCache[c][i])->cla - 1 + i * vocab->getCL(0)]++;
		else
			outputV[c][batchCache[c][i] - vocab->getCB(c) + i * vocab->getCL(c)]++;
	}
    
    /*Update biasO
    for (int i = 0;i < ba;i++) {
        CBLAS_AXPY(vocab->getCL(c), alpha_n, outputV[c] + i * vocab->getCL(c), 1, biasO + vocab->getCB(c), 1);
    }
    CBLAS_AXPY(vocab->getCL(c), -beta, biasO + vocab->getCB(c), 1, biasO + vocab->getCB(c), 1);
    */
    //printf("class%d after update:", c);
    //gvprint("biasO", vocab->getCL(c), biasO + vocab->getCB(c));


	CBLAS_GEMM(CblasColMajor, CblasNoTrans, CblasNoTrans, hidden_size, ba, vocab->getCL(c), 1, weightHO + vocab->getCB(c) * hidden_size, hidden_size, outputV[c], vocab->getCL(c), 0, hiddenError, hidden_size); //beta is zero, propagate output error to hidden
    
    glib_vsigerr(ba * hidden_size, hiddenCache[c] + hidden_size * batch_size, auxV, oneV);
	VMUL(ba * hidden_size, hiddenError, auxV, hiddenError); //get hiddenError
	constrainWeights(ba * hidden_size, hiddenError);
	CBLAS_GEMM(CblasColMajor, CblasNoTrans, CblasTrans, hidden_size, vocab->getCL(c), ba, alpha_n, hiddenCache[c] + hidden_size * batch_size, hidden_size, outputV[c], vocab->getCL(c), 1 - beta * alpha_n, weightHO + vocab->getCB(c) * hidden_size, hidden_size); //update weightHO

    CBLAS_COPY(hidden_size * hidden_size, weightVH[0], 1, weightVH_back[0], 1); //backup weightVH
    if (nnlm) 
        for (int i = 1;i < bptt + 1;i++)
            CBLAS_COPY(hidden_size * hidden_size, weightVH[i], 1, weightVH_back[i], 1);
    else
        CBLAS_COPY(hidden_size * hidden_size, weightHH, 1, weightHH_back, 1); //backup weightHH
   
    for (int time = 1;time <= bptt + 1;time++) { 
        if (time != 1 && !nnlm) { //first, only update the current wordvec using the current hiddenError, in nnlm, hiddenError won't be changed!
		    CBLAS_GEMM(CblasColMajor, CblasNoTrans, CblasTrans, hidden_size, hidden_size, ba, alpha_n, hiddenError, hidden_size, hiddenCache[c] + time * batch_size * hidden_size, hidden_size, 1 - beta * alpha_n, weightHH, hidden_size); //W = (1 - beta) W + alpha Error * Hidden
		    CBLAS_GEMM(CblasColMajor, CblasTrans, CblasNoTrans, hidden_size, ba, hidden_size, 1, weightHH_back, hidden_size, hiddenError, hidden_size, 0, auxV, hidden_size); //e_h(t-1) = W^T * e_h(t) .* s(t - 1) .* (1 - s(t - 1)), since the beta in CLBAS_GEMM is zero, I can not directly use hiddenError as the result address
	    	CBLAS_COPY(ba * hidden_size, auxV, 1, hiddenError, 1);
    		glib_vsigerr(ba * hidden_size, hiddenCache[c] + time * hidden_size * batch_size, auxV, oneV);
    		VMUL(ba * hidden_size, hiddenError, auxV, hiddenError);
		    constrainWeights(ba * hidden_size, hiddenError);
        }

        int vhNow = nnlm ? time - 1 : 0;
        //update weightVH
        if (tie == 1 || nnlm) { //The VH is initialized to be identity matrix when nnlm == 0 and tie == 0, no need to update it
            getWordVec(ba, batchCache[c] + time * batch_size, auxV, 0);
            CBLAS_GEMM(CblasColMajor, CblasNoTrans, CblasTrans, hidden_size, hidden_size, ba, alpha_n, hiddenError, hidden_size, auxV, hidden_size, 1 - beta * alpha_n, weightVH[vhNow], hidden_size); 
        }

        CBLAS_GEMM(CblasColMajor, CblasTrans, CblasNoTrans, hidden_size, ba, hidden_size, 1, weightVH_back[vhNow], hidden_size, hiddenError, hidden_size, 0, auxV, hidden_size);
		for (int i = 0;i < ba;i++) { //Propagate to word vector
			//printf("tr|%s ", vocab->getWord(batchHis[postt * batch_size + i])->word);
			if (batchCache[c][time  * batch_size + i] == -1) { //Stream ends
				for (int j = 0;j < hidden_size;j++)
                    hiddenError[i * hidden_size + j] = 0;
                continue;
            }
            updateWordVec(batchCache[c][time * batch_size + i], auxV + i * hidden_size, alpha_n);
            /*
			WEIGHT_TYPE *wvec = vocab->getWord(batchCache[c][time * batch_size + i])->vec;
			for (int j = 0;j < hidden_size;j++)
				wvec[j] = wvec[j] * (1 - beta * alpha_n) + alpha_n * auxV[i * hidden_size + j];
            */
			if (batchCache[c][time * batch_size + i] == senId && independent) //At sentence break, clean the error vector
                glib_vSetZero(hidden_size, hiddenError + i * hidden_size);
    	}
	}
    profiler->stop("XRnn::trainClass");
}
void XRnn::propagateHH(int pl, int pn, WEIGHT_TYPE *hiddenV, int *batch) { //For RNN
	if (pl != -1) //Propagate from last time 
		CBLAS_GEMM(CblasColMajor, CblasNoTrans, CblasNoTrans, hidden_size, batch_size, hidden_size, 1, weightHH, hidden_size, hiddenV + (hidden_size * batch_size * pl), hidden_size, 0, hiddenV + (hidden_size * batch_size * pn), hidden_size);
    else { //let it be zero
        glib_vSetZero(hidden_size * batch_size, hiddenV + hidden_size * batch_size * pn);
    }

    for (int i = 0;i < batch_size;i++) { //put the wordVec to auxV
        if (batch[i] == -1) {
            glib_vSetZero(hidden_size, auxV + hidden_size * i);
            continue;
        }
        if (independent && (pl != -1) && batch[i] == senId) {
           glib_vSetZero(hidden_size, hiddenV + hidden_size * batch_size * pn + hidden_size * i);
        }
        getWordVec(1, batch + i, auxV + hidden_size * i, 0);
    }
    CBLAS_GEMM(CblasColMajor, CblasNoTrans, CblasNoTrans, hidden_size, batch_size, hidden_size, 1, weightVH[0], hidden_size, auxV, hidden_size, 1, hiddenV + (hidden_size * batch_size * pn), hidden_size);

    /* bias for hidden layer, I decided later to remove it
    for (int i = 0;i < batch_size;i++)
       VADD(hidden_size, hiddenV + (hidden_size * batch_size * pn) + hidden_size * i, biasH, hiddenV + (hidden_size * batch_size * pn) + hidden_size * i);
    */
	glib_vsigmoid(hidden_size * batch_size, hiddenV + (hidden_size * batch_size * pn), hiddenV + (hidden_size * batch_size * pn), oneV); //Actvation function on the hidden layer
}
void XRnn::propagateVH(int pn, WEIGHT_TYPE *hiddenV, int *batchHis) {
    int pw = pn; //Word his
    for (int l = 0;l < bptt + 1;l++) {
        getWordVec(batch_size, batchHis + pw * batch_size, auxV, 0);

        if (independent) //Clear when meet </s>
            for (int i = 0;i < batch_size;i++)
                if (batchHis[pw * batch_size + i] == senId)
                    glib_vSetZero(hidden_size, hiddenV + hidden_size * batch_size * pn + hidden_size * i);

        CBLAS_GEMM(CblasColMajor, CblasNoTrans, CblasNoTrans, hidden_size, batch_size, hidden_size, 1, weightVH[l], hidden_size, auxV, hidden_size, l == 0 ? 0 : 1, hiddenV + (hidden_size * batch_size * pn), hidden_size);

        pw = pw - 1 < 0 ? bptt + 1 : pw - 1;
    }
    glib_vsigmoid(hidden_size * batch_size, hiddenV + (hidden_size * batch_size * pn), hiddenV + (hidden_size * batch_size * pn), oneV); //Actvation function on the hidden layer
}
void XRnn::propagateHO(WEIGHT_TYPE *hiddenV, int c) {
	if (c > vocab->cn())
		gerr("XRnn::propagateHO meet invlaid class %d.\n", c);
	//gvprint("hiddenVector", batch_size * hidden_size, hiddenHis + batch_size * hidden_size * posNow);
	//gvprint("weightHO", output_size * hidden_size, weightHO);
	
	//gvprint("debug XRnn::propagateHO hiddenV", batch_size * hidden_size, hiddenV); 

	CBLAS_GEMM(CblasColMajor, CblasTrans, CblasNoTrans, vocab->getCL(c), batch_size, hidden_size, 1, weightHO + vocab->getCB(c) * hidden_size, hidden_size, hiddenV, hidden_size, 0, outputV[c], vocab->getCL(c)); //weightHO is hidden_size * output_size

	//do the softmax
	for (int i = 0;i < batch_size;i++) {
        //VADD(vocab->getCL(c), outputV[c] + i * vocab->getCL(c), biasO + vocab->getCB(c), outputV[c] + i * vocab->getCL(c)); adding biasO did not show improvement
    	glib_vsoftmax(vocab->getCL(c), outputV[c] + i * vocab->getCL(c), outputV[c] + i * vocab->getCL(c));
	}
}
double XRnn::getProb(int batch_id, int word_id) {
	int cla = vocab->getWord(word_id)->cla;
	//glog(1, "c%lf-w%lf ", outputV[0][batch_id * vocab->getCL(0) + cla - 1],  outputV[cla][batch_id * vocab->getCL(cla) + word_id - vocab->getCB(cla)]);
   return outputV[0][batch_id * vocab->getCL(0) + cla - 1] * outputV[cla][batch_id * vocab->getCL(cla) + word_id - vocab->getCB(cla)];
}
void XRnn::getWordVec(int n, int *w, WEIGHT_TYPE *t, bool add) {
    for (int l = 0;l < n;l++) {
        if (w[l] == -1)
            for (int j = 0;j < hidden_size;j++)
                t[l * hidden_size + j] += (add == 0) ? -t[l * hidden_size + j] : 0;
        else {
            int k = w[l];
            WEIGHT_TYPE *wvec = vocab->getWord(k)->vec;
            for (int j = 0;j < hidden_size;j++)
                t[l * hidden_size + j] += (add == 0) ? -t[l * hidden_size + j] + wvec[j] : wvec[j];
       }
    }
}
void XRnn::updateWordVec(int w, WEIGHT_TYPE *error, double alpha_n) {
    WEIGHT_TYPE *wvec = vocab->getWord(w)->vec;
    for (int j = 0;j < hidden_size;j++)
        wvec[j] = wvec[j] * (1 - beta * alpha_n) + alpha_n * error[j];
}
void XRnn::zeroWordVector(int thres) {
    glog(1, "XRnn::zeroWordVector zeroing input and output word vector whose count is less than %d...\n", thres);
    int wz = 0; //How many words have been zeroed. 
    for (int i = 0;i < vocab->size();i++) {
        GWord *w = vocab->getWord(i);
        if (w->count >= thres)
            continue;
        wz++;
        glog(4, "XRnn::zeroWordVector zeroing word %s\n", w->word);
        glib_vSetZero(hidden_size, w->vec);
        glib_vSetZero(hidden_size, weightHO + i * hidden_size); 
    }
    glog(1, "XRnn::zeroWordVector zeroed %d words.\n", wz);
}
