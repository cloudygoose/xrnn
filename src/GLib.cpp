#include "GLib.h"
#include "mkl.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "unistd.h" //For usleep function
#include <stdarg.h>
#include <math.h>
#include <time.h>

const int MAX_WORD_LENGTH = 50;
const int VOCAB_HASH_SIZE = 100000000; //10^8
const double TOO_SMALL_PROB = 0.000000001; //1e-9
const int FEED_CONSECUTIVE_SENTENCE = 1000;
//const int VOCAB_HASH_SIZE = 1000000; //10^6
int debug_level; //Set in main() 
int debug_pause; //Set in main()
int warn_pause;

void glog(int log_level, const char *s, ...) {
    if (log_level <= debug_level) {
        va_list argptr;
        va_start(argptr, s);
        vfprintf(stdout, s, argptr);
		fflush(stdout);
        va_end(argptr);
		usleep(debug_pause);
    }
}

char* ggettime(char *buffer) {
	tm *temptm;  
	time_t temptime;  
	time(&temptime);
	temptm = localtime(&temptime);  
	sprintf(buffer, "%02d:%02d:%02d %d-%02d-%02d", temptm->tm_hour,temptm->tm_min,temptm->tm_sec, temptm->tm_year+1900,temptm->tm_mon+1,temptm->tm_mday);
	return buffer;
}
void gbiglog(int log_level, const char *s, ...) {
    if (log_level <= debug_level) {
		int band_length = 20;
		char buffer[200];
        va_list argptr;
        va_start(argptr, s);
        vsprintf(buffer, s, argptr);
		va_end(argptr);

		if (buffer[strlen(buffer) - 1] != '\n')
			strcat(buffer, "\n");

		if (strlen(buffer) + 5 > band_length)
			band_length = strlen(buffer) + 5;

		for (int i = 0;i < band_length;i++) printf("#"); printf("\n");
		printf("## "); printf("%s", buffer);
        for (int i = 0;i < band_length;i++) printf("#"); printf("\n");

    }
}
void gerr(const char *s, ...) {
    printf("[ERROR] : ");
    va_list argptr;
    va_start(argptr, s);
    vfprintf(stdout, s, argptr);
    va_end(argptr);
	fflush(stdout);
    exit(1);
}
void gwarn(const char *s, ...) {
    printf("[WARNING] : ");
    va_list argptr;
    va_start(argptr, s);
    vfprintf(stdout, s, argptr);
    va_end(argptr);
	if (warn_pause > 0)
		usleep(warn_pause);
}
float grand(float min, float max) {
    return rand()/(float)RAND_MAX*(max-min)+min;
}
void gvprint(char *me, int n, WEIGHT_TYPE *a) {
	printf("%s ", me);
	for (int i = 0; i < n;i++)
		printf("%.7f ", a[i]);
	printf("\n");
	if (debug_pause > 0)
		usleep(debug_pause);
}
void glib_vsigmoid(int n, const WEIGHT_TYPE *a, WEIGHT_TYPE *b, WEIGHT_TYPE *ones) {
	CBLAS_COPY(n, a, 1, b, 1);	
	SCAL(n, -1, b, 1);	
	VEXP(n, b, b);		
	VADD(n, b, ones, b);		
	VINV(n, b, b);
}
void glib_vsoftmax(int n, const WEIGHT_TYPE *a, WEIGHT_TYPE *b) {
	VEXP(n, a, b);
	WEIGHT_TYPE ss = CBLAS_ASUM(n, b, 1);
	SCAL(n, 1 / ss, b, 1);	
}
void glib_vsigerr(int n, const WEIGHT_TYPE *a, WEIGHT_TYPE *b, WEIGHT_TYPE *ones) {
	if (a == b)
		gerr("glib_vsigerr (WEIGHT_TYPE*)a and (WEIGHT_TYPE*)b should not point to the same address\n");

	CBLAS_COPY(n, a, 1, b, 1);
	SCAL(n, -1, b, 1);
	VADD(n, b, ones, b);
	VMUL(n, a, b, b);		
}
void glib_splitWordUTF8(const char *s) {
    char buffer[MAX_WORD_LENGTH];
    printf("length of s [%s] : %d\n|", s, strlen(s));
    int i = 0;
    while (i < strlen(s)) {
        int l = i, kk = 128, j = 0;
        while ((s[l] & kk) == kk) {
            buffer[j++] = s[i++];
            kk = kk / 2;
        }
        if (j == 0)
            buffer[j++] = s[i++];
        buffer[j] = 0;
        printf("%s|", buffer);
    }
    printf("\n");
}
void glib_vSetZero(int n, WEIGHT_TYPE *a) {
    for (int i = 0;i < n;i++)
        a[i] = 0;
}
void glib_checkHiddenAct(int n, WEIGHT_TYPE *a, char *mes) {
    for (int i = 0;i < n;i++) {
        if (isnan(a[i]) || a[i] < -0.01 || a[i] > 1.01) {
            printf("%s wrong hidden Act at %d is %lf\n", mes, i, a[i]);
            exit(1);
        }
    }
}
