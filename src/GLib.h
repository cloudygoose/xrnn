#ifndef _GLIB_
#define _GLIB_

//*************CHOOSE WEIGHT TYPE HERE*************

//Only choose one
//#define WEIGHT_TYPE_BE_FLOAT 
#define WEIGHT_TYPE_BE_DOUBLE

#ifdef WEIGHT_TYPE_BE_FLOAT
    #define WEIGHT_TYPE_STR "FLOAT"
	typedef float WEIGHT_TYPE; //WEIGHT_TYPE affects the precision of hidden activations, word-vectors
	#define WEIGHTSIGN "%f"
	#define CBLAS_GEMM cblas_sgemm //mkl's general matrix operation
	#define CBLAS_ASUM cblas_sasum //mkl's vector summation
	#define CBLAS_COPY cblas_scopy //mkl's vector copy
	#define VEXP vsExp //mkl's vector exp
	#define SCAL cblas_sscal //mkl's product to a vector by a scalar
	#define VADD vsAdd //mkl's vector addition
	#define VINV vsInv //mkl's vector element-wise inversion
	#define VMUL vsMul //mkl's vector element-wise multiplication
    #define CBLAS_AXPY cblas_saxpy //Computes a vector-scalar product and adds the result to a vector
    #define MKL_CSRMM mkl_scsrmm //
#endif

#ifdef WEIGHT_TYPE_BE_DOUBLE
    #define WEIGHT_TYPE_STR "DOUBLE"
	typedef double WEIGHT_TYPE;
	#define WEIGHTSIGN "%lf"
	#define CBLAS_GEMM cblas_dgemm
	#define CBLAS_ASUM cblas_dasum
	#define CBLAS_COPY cblas_dcopy
	#define VEXP vdExp
	#define SCAL cblas_dscal
	#define VADD vdAdd
	#define VINV vdInv
	#define VMUL vdMul
    #define CBLAS_AXPY cblas_daxpy
    #define MKL_CSRMM mkl_dcsrmm
#endif

//*************************************************

extern const int MAX_WORD_LENGTH;
extern const int VOCAB_HASH_SIZE;
extern const double TOO_SMALL_PROB;
extern const int FEED_CONSECUTIVE_SENTENCE;
extern int debug_level;
extern int debug_pause;
extern int warn_pause;
void glog(int log_level, const char *s, ...);
void gvprint(char *me, int n, WEIGHT_TYPE *a); //First print the message, then the vector, ends with a line break.
char* ggettime(char *buffer); //Print the current time and date into buffer, returns the buffer
void gbiglog(int log_level, const char *s, ...); //Give a big, good display of the message
void gwarn(const char *s, ...);
void gerr(const char *s, ...); //Display the error message, and exit program
float grand(float min, float max);
void glib_vsigmoid(int n, const WEIGHT_TYPE* a, WEIGHT_TYPE *b, WEIGHT_TYPE *one); //Apply the sigmoid function to n elements from a, store the results in b, the user should provide a vector full of one
void glib_vsoftmax(int n, const WEIGHT_TYPE *a, WEIGHT_TYPE *b); //Apply the softmax operation to n elements from a, store the results in b
void glib_vsigerr(int n, const WEIGHT_TYPE *a, WEIGHT_TYPE *b, WEIGHT_TYPE *ones); //Calculate the gradient of the sigmoid function for a, which is element-wise x*(1-x), the result is stored in b, attention that this function requires that a != b
void glib_splitWordUTF8(const char *s);
void glib_vSetZero(int n, WEIGHT_TYPE *a); //set the vector to zero
void glib_checkHiddenAct(int n, WEIGHT_TYPE *a, char *mes);
#endif
