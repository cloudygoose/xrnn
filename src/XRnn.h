#ifndef _XRNN_
#define _XRNN_

#include <vector>
#include <list>

#include "GOpVec.h"
#include "GVocab.h"
#include "XNFeed.h"
#include "GLib.h"
#include "XProfiler.h"

class TestRes {
	private:
		double logp_net;
		double logp_net_oovpart;

		double logp_other;
		double logp_other_oovpart;
		
		double logp_combine;
		double logp_combine_oovpart;

		int wordcn, sencn, oovcn;
		int independent;
	public:
		TestRes(double d1, double d2, int wc, int sc, int oc);
		TestRes(double d1, double d2, double d3, double d4, double d5, double d6, int wc, int sc, int oc);
		double giveNetPPL();
		double giveNetPPLwithOOV();
		double giveOtherPPL();
		double giveOtherPPLwithOOV();
		double giveCombinePPL();
		double giveCombinePPLwithOOV();
};

class XRnn {
	private:
		int batch_size;
        bool tie; 
        bool nnlm;
		int log_number; //Print a T after log_number words are processed
		int hidden_size;
		GVocab *vocab;
        XProfiler *profiler;
		GOpVec *opVec;
		int bptt;
		bool initialized;
		int output_size; //Should be inferred from vocab->size()
	
		float alpha, beta;
	
		WEIGHT_TYPE *hiddenHis; //Save hidden layer's activation history
		int *batchHis; //Save the history of mini-batches
		int independent;
	
		int *batchNow; //Store the word ids for the current batch
		int posNow, posLast; //Store the position index in the history cache	
		
		int senId; //The Id of </s> in the vocab
		int unkId; //The Id of <unk> in the vocab
		int unk_div; //The prob of <unk> will be divided by unk_div, usually 1

		WEIGHT_TYPE weightConstrain; //Weights whose abs are larger than weight_constrain(option) will be reduced to weight_constrain * 0.9, should be positive.

		WEIGHT_TYPE *weightHO; //All transformation matrices are stored in column-major style, weightHO is hidden_size * output_size
        WEIGHT_TYPE *biasO; //The bias vector on the output layer, now removed
		WEIGHT_TYPE *weightHH; //HiddenV(t) += weightHH * HiddenV(t-1)
		WEIGHT_TYPE *weightHH_back; //Used as back-ups for weightHH for doing bptt
        WEIGHT_TYPE *weightHC; //Transformation matrix from hidden to character output layer
        WEIGHT_TYPE **weightVH; //HiddenV(t) += weightVH[0] * wordvec(t)
        WEIGHT_TYPE **weightVH_back;
		WEIGHT_TYPE **outputV; //outputV[i] stores the outputs for class i, its size is size(class i) * batch_size
		WEIGHT_TYPE **hiddenCache; //hiddenCache[i] acts like a cache, stores hidden-layer activations when the next target belongs to class_i
		int **batchCache; //batchCache[i] acts like a cache, stores the word_ids in the batch
		int *cacheCount; //How many instances have been stored in the cache
		WEIGHT_TYPE *hiddenError; //Used for store the error on the hidden layer, its length is hidden_size * batch_size, col-major
		WEIGHT_TYPE *oneV; //The vector will have length of (batch_size * max(hidden_size, output_size)), and be full of 1, used for the sigmoid calcuation, and back-propagation

        WEIGHT_TYPE *auxV2; //An auxiliary vector to store intermediate results, its length will be (batch_size * max(hidden, output_size))
        WEIGHT_TYPE *auxV; //An auxiliary vector to store intermediate results, its length will be (batch_size * max(hidden, output_size))

		int checkProb(double *prob, char *word); //If this probability falls below a certain threshold, set it to the threshold and give a warning(debug_level 3), return 1.If it is nan, give an error and exit.If normal, return 0
		double getProb(int batch_id, int word_id); //Get the probability from the outputV after rnnPropagate()
		void constrainWeights(int len, WEIGHT_TYPE *m); //Constrain the weights.

		void debugMatrix(const char *name, int len, WEIGHT_TYPE *m); //Active only debug_level >= 3, check whether a matrix contains nan, if so, exit
		void checkProbV(int len, WEIGHT_TYPE *pv); //Active only debug_level >= 3, check whether a probability vector contains nan, contains negative values, and sum up to one.
		void makeSpace(); //Allocate memory for matrices when scale is set.
		void trainClass(int c, double &logp, int &tooSmallProb, int &wordcn, int &sencn); //Train the class c part using histories stored in the cache
		int *trainClassCount; //Count how many batches are trained for each class
        void updateWordVec(int w, WEIGHT_TYPE *error, double alpha_n); //Update word w with error
	public:
		XRnn(GOpVec *ops, GVocab *voc);
		void propagateHH(int pl, int pn, WEIGHT_TYPE *hiddenV, int *batch); //Propagate from pl(posLast) to pn(posNow), and the batch, for RNN
        void propagateVH(int pn, WEIGHT_TYPE *hiddenV, int *batch); //Get the hidden vector, for NNLM
		void propagateHO(WEIGHT_TYPE *hiddenV, int c); //Propagate from hidden hiddenV to output, for class c
		void initialize(); //Initialize weightHO and weightHH
		void initializeByFile(); //Initialize vocab, weightHH, weightHO using the file named by the read_lm_file option
		TestRes* propagateOnFile(const char *fn); //Calculate probability for the the assigned file
		TestRes* propagateOnFileInterpolate(const char *fn, double inter_this, const char *otherp_fn); //Interpolate with other probability generated by SRILM followed by convert
		void trainOneIteration(); //Train the model over the train_file for one iteration
		void printModel(); //Print the model to the file named by output_lm_file.
        void getWordVec(int n, int *w, WEIGHT_TYPE *t, bool add); //Concatanate the word vectors to t, "add" specifies whether add the vec to target or copy it 
        void zeroWordVector(int thres); //Set wordvector whose (count < thres) to zero, both for input and output layer
};

#endif
