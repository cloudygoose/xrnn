#include "GLib.h"
#include "GVocab.h"
#include "GOpVec.h"

#include <stdio.h>
#include <vector>
#include <list>
#ifndef _XNFEED_H_
#define _XNFEED_H_

extern const int MAX_WORD_LENGTH;

class XNFeed {
	private:
		GVocab *vocab;
		int batch_size;
		FILE *file;
		bool refreshStream(int ind); //If stream ind is empty, grab another sentence from file, return whether the refreshed stream can continue.
		std::vector<std::list<int>*> *streams;
		int independent;
	public:
		XNFeed(int bs, int ind, GVocab *voc);
		void initialize(const char *fn); //Set XNFeed to open the file fn
		bool nextBatch(int *des); //Form a mini-batch from the head of streams and step forward the streams, return if successful, if ending, the file will be closed by refreshStream(int)
};

#endif


