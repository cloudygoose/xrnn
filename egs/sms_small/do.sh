set -e #exit when an error occurs
SWORKD=./work
DATAD=./SMSdata
TRAINF=$DATAD/sms.50k.train
VALIDF=$DATAD/sms.50k.valid
TESTF=$DATAD/sms.test
WVOCAB=$DATAD/wlist.txt.40000.sms
CVOCAB=$DATAD/wlist.txt.40000.sms.char.nospace
UNK_DIV=13432
GDB="gdb -ex=r --args"
VAL="valgrind --tool=memcheck"

echo "SLEEP=${SLEEP:=1} sleep $SLEEP seconds..."
echo "training parameters:"
echo "HIDDEN=${HIDDEN:=100}"
echo "CLASS=${CLASS:=100}"
echo "MIN_ITER=${MIN_ITER=10}"
echo "BT=${BT:=1} BPTT"
echo "TIE=${TIE:=0}"
echo "SPLITI=${SPLITI:=0}"
echo "WCSI=${WCSI:=0}"
echo "WCSO=${WCSO:=0}"
echo "SPLITO=${SPLITO:=0}"
echo "CHARPM=${CHARPM:=0}"
echo "DP=${DP:=0} debug pause"
echo "BATCH=${BATCH:=10}"
echo "VF=${VF:=1} valid_first"
echo "SHUF=${SHUF:=1} shuf the training file"
echo "WC=${WC:=15} default weight constrain is 15"
echo "IND=${IND:=1} independent"
echo "DL=${DL:=2} debug_level default 2"
echo "scheduled parameters:"
echo "parameters for schduling:"
echo "SLR=${SLR:=0.05} default learning rate is 0.05"
echo "BETA=${BETA:=1e-6} beta default is 1e-6"
echo "HR=${HR:=0.5} half rate"
echo "begin from STEP $STEP, press any key to confirm..."
read rub

ME="[[SCHEDULER]]"
echo "$ME SLEEP $SLEEP seconds"
sleep $SLEEP

function trainXRNN {
	NAME=h${HIDDEN}c${CLASS}tie${TIE}si${SPLITI}wcsi${WCSI}so${SPLITO}wcso${WCSO}cpm${CHARPM}lr${SLR}hr${HR}bt${BT}ind${IND}beta${BETA}bat${BATCH}wc${WC}shuf${SHUF}
    if [[ $SHUF == 1 && $IND == 0 ]]; then
        echo "$ME WARNING: both option SHUF == 1 and IND == 0, this does not make sense."
        sleep 3
    fi
	LR=$SLR
	WORKD=$SWORKD"/exps/$NAME"
    if [[ -d $WORKD ]]; then
        echo "$ME working-dir exists, rm $WORKD in 3 seconds..."
        sleep 3
        rm -r $WORKD
    fi
    CVOCAB_USE=$CVOCAB
    if [[ $CHARPM == 1 ]]; then
        echo "char_mp is set, change to $CVOCAB"".pm"
        CVOCAB_USE=$CVOCAB".pm"
        sleep 3
    fi
	mkdir -p $WORKD
    echo "$ME (to use latest version) cp --verbose ../../bin/XRNN_* $WORKD/"
    cp --verbose ../../bin/XRNN_* $WORKD/
    START_DECAY=0
	STOP=0

    echo "$ME copying TRAINF to WORKD/trainf..."    
    cp --verbose $TRAINF $WORKD/trainf
    sleep 1

	COMM="$WORKD/XRNN_train -tie $TIE -alpha $LR -beta ${BETA} -spliti $SPLITI -update_wcsi $WCSI -update_wcso $WCSO -splito $SPLITO -count_vocab $TRAINF -limit_vocab $WVOCAB -cvocab $CVOCAB_USE -char_pm $CHARPM -debug_level $DL -sort_vocab 1 -bptt $BT -print_vocab $WORKD/vocab -independent $IND -iter_number 0 -batch_size ${BATCH} -weight_constrain $WC -rand_seed 2 -debug_level $DL -train_file $WORKD/trainf -valid_file $VALIDF -hidden_size $HIDDEN -class $CLASS -valid_first $VF -debug_pause $DP -warn_pause 100000 -output_lm_file $WORKD/lm_file_${NAME}"
    echo "$ME $COMM" | tee -a $WORKD/LOG_${NAME}
    eval $COMM | tee $WORKD/LOG_${NAME} 

	LLOGP=$(grep logp-for-this-file $WORKD/LOG_${NAME} | tail -1 | cut -d ' '  -f 3)
	cp $WORKD/lm_file_${NAME} $WORKD/lm_file_${NAME}_ITER00_CV${LLOGP}_LR${LR}
	echo "$ME snapshot $WORKD/lm_file_${NAME}_ITER00_CV${LLOGP}_LR${LR} saved..."
	ITER=0
	while [[ 1 == 1 ]]; do
		ITER=$(($ITER + 1))
		if [[ $ITER -gt $MIN_ITER && $STOP == 1 ]]; then
			echo "$ME ITER > $MIN_ITER && STOP == 1, stopping..."
			break
		fi
		if [[ ! -f $WORKD/lm_file_${NAME} ]]; then
			echo "$ME lm_file not found, exiting..."
			exit
		fi
        if [[ $SHUF == 1 ]]; then
            echo "$ME shuffling TRAINF"
            cat $TRAINF | awk 'BEGIN{srand('$ITER');}{print rand()"\t"$0}' | sort -k1 -n | cut -f2- > $WORKD/trainf
        fi
		COMM="$WORKD/XRNN_train -tie $TIE -spliti $SPLITI -update_wcsi $WCSI -update_wcso $WCSO -splito $SPLITO -alpha $LR -beta ${BETA} -count_vocab $TRAINF -debug_level $DL -bptt $BT -independent $IND -iter_number $ITER -batch_size ${BATCH} -weight_constrain $WC -rand_seed 2 -debug_level 2 -train_file $WORKD/trainf -valid_file $VALIDF -hidden_size $HIDDEN -class $CLASS -debug_pause 0 -warn_pause 100000 -read_lm_file $WORKD/lm_file_${NAME} -cvocab $CVOCAB_USE -char_pm $CHARPM -output_lm_file $WORKD/lm_file_${NAME}_tmp"
        echo "$ME $COMM" | tee -a $WORKD/LOG_${NAME}
        eval $COMM | tee -a $WORKD/LOG_${NAME}
		LOGP=$(grep logp-for-this-file $WORKD/LOG_${NAME} | tail -1 | cut -d ' '  -f 3)
		COPYF=$WORKD/lm_file_${NAME}_ITER$(printf "%02d" "$ITER")_CV${LOGP}_LR${LR}

        if [[ $(awk 'BEGIN { print('$LOGP' < '$LLOGP') }') == 1 ]]; then
            echo "$ME $LOGP < $LLOGP, won't use new model..."
			COPYF=${COPYF}RESTORED
        else
            echo "$ME $LOGP > $LLOGP, use new model..."
            cp $WORKD/lm_file_${NAME}_tmp $WORKD/lm_file_${NAME}
        fi
		
		cp $WORKD/lm_file_${NAME} $COPYF #save the snapshot of the current iteration
		echo "$ME snapshot $COPYF saved..."

		if [[ $(awk 'BEGIN { print('$LOGP' * 1.003 < '$LLOGP') }') == 1 ]]; then
			if [[ $START_DECAY == 1 ]]; then
				echo "$ME $LOGP * 1.003 < $LLOGP(again), but still continue until min_iter..."
				STOP=1
			fi
			echo "$ME $LOGP * 1.003 < $LLOGP, starting lr decay..."
			START_DECAY=1
		else
			echo "$ME $LOGP * 1.003 > $LLOGP, ok..."
		fi
        LLOGP=$LOGP
		if [[ $START_DECAY == 1 ]]; then
			echo "$ME LR($LR) = LR * $HR"
			LR=$(awk 'BEGIN { print('$LR' * '$HR')}')
            echo "$ME LR changed to $LR"
		fi
	done
    
    COMM="$WORKD/XRNN_test -cvocab $CVOCAB_USE -tie $TIE -spliti $SPLITI -splito $SPLITO -hidden_size $HIDDEN -batch_size 1 -count_vocab $TRAINF -class $CLASS -independent $IND -char_pm $CHARPM -ppl_file $TESTF -read_lm_file $WORKD/lm_file_${NAME} -unk_div $UNK_DIV"
    echo $COMM | tee -a $WORKD/LOG_${NAME}
    eval $COMM | tee -a $WORKD/LOG_${NAME}	
	echo print results to ./RESULT
	echo "----------RESULT----$(date)-----RESULT----------" |tee -a ./RESULT
	ls $WORKD | tee -a ./RESULT
	tail $WORKD/LOG_${NAME} | tee -a ./RESULT
}
trainXRNN
#BT=1
#trainXRNN

