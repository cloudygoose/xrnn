ngram-count -text ptb.train.txt -order 4 -lm templm -gt3min 1 -gt4min 1 -kndiscount -interpolate -unk
ngram -lm templm -order 4 -ppl ptb.test.txt -debug 2 -unk > temp.ppl
rm templm
awk ' { if ($1 == "p(" && $3 == "|"  && $NF == "]") { print $(NF-3), $2 } } ' temp.ppl > ngram.prob
echo "Note : If meets oov(<unk>), the prob is zero."

